with Cargame.Globals;

package body Cargame.Gameplay is

   type Lane is new Integer range 1 .. 3;

   Lane_X : constant array (Lane) of Single := (1 => -1.0, 2 => 0.0, 3 => +1.0);

   procedure Spawn_Obstacle(L : in Lane) is
   begin
      Globals.Entities.Append
	((
	  RVAO     => Globals.RVAOs.Barrel,
	  Position => (X => Lane_X_Positions(L),
		       Y => 0.0,
		       Z => -10.0 * Globals.RVAOs.Car.Volume(Z))
	 ));
   end Spawn_Obstacle;

   procedure Advance_Obstacle(E : in out Entity);

   procedure Change_Lanes(E : in out Entity; Dir : Change_Direction) is
      X : constant Single := E.Position(X);
   begin
      if X = Lane_X(1) or else X < Lane_X(2) then
	 E.Position(X) := (if Dir = Left then Lane_X(1) else Lane_X(2));
      elsif X = Lane_X(2) then
	 E.Position(X) := (if Dir = Left then Lane_X(1) else Lane_X(3));
      elsif X = Lane_X(3) or else X > Lane_X(2) then
	 E.Position(X) := (if Dir = Left then Lane_X(2) else Lane_X(3));
      end if;
   end Change_Lanes;

   function Entities_Collide(A, B : in Entity) return Boolean is

      A_Scaled_Z : constant Single := A.Scale * (A.RVAO.Volume.Z / 2);
      B_Scaled_Z : constant Single := B.Scale * (B.RVAO.Volume.Z / 2);

      A_Near     : constant Single := A.Position(Z) - A_Scaled_Z;
      A_Far      : constant Single := A.Position(Z) + A_Scaled_Z;
      B_Near     : constant Single := B.Position(Z) - B_Scaled_Z;
      B_Far      : constant Single := B.Position(Z) + B_Scaled_Z;

      type A_Range is new Single range A_Near .. A_Far;
      type B_Range is new Single range B_Near .. B_Far;

   begin
      -- If they're not in the same lane, they don't collide.
      if A.Position(X) /= B.Position(X) then return False; end if;
      -- Should be on the same height.
      if A.Position(Y) /= B.Position(Y) then return False; end if;

      return (          A_Near in B_Range or else A_Far in B_Range
		or else B_Near in A_Range or else B_Far in A_Range);

   end Entities_Collide;

   function Entity_Is_Off_Screen(E : in Entity) return Boolean;

   procedure Clean_Up_Invisible_Entities;

end Cargame.Gameplay;
